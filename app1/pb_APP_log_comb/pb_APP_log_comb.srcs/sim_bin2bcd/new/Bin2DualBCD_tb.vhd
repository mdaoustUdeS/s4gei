----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/07/2019 08:34:20 PM
-- Design Name: 
-- Module Name: testBench - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
LIBRARY UNISIM;
USE UNISIM.Vcomponents.ALL;

ENTITY Bin2DualBCD_tb IS
END Bin2DualBCD_tb;


ARCHITECTURE behavioral OF Bin2DualBCD_tb IS 

COMPONENT Bin2DualBCD
   PORT(
        ADCbin : in STD_LOGIC_VECTOR (3 downto 0);
        Dizaines : out STD_LOGIC_VECTOR (3 downto 0);
        Unites_ns : out STD_LOGIC_VECTOR (3 downto 0);
        Code_signe : out STD_LOGIC_VECTOR (3 downto 0);
        Unite_s : out STD_LOGIC_VECTOR (3 downto 0)
    );
END COMPONENT;
   
--> G�n�rez des signaux internes au test bench avec des noms associ�s et les m�me types que dans le port
    -- Note: les noms peuvent �tre identiques, dans l'exemple on a ajout� un suffixe pour
    -- identifier clairement le signal qui appartient au test bench

   SIGNAL adc_sim         : STD_LOGIC_VECTOR (3 DOWNTO 0);
   SIGNAL sortie_dizaines    : STD_LOGIC_VECTOR (3 DOWNTO 0);
   SIGNAL sortie_unites_ns    : STD_LOGIC_VECTOR (3 DOWNTO 0);
   SIGNAL sortie_code_signe    : STD_LOGIC_VECTOR (3 DOWNTO 0);
   SIGNAL sortie_unite_s    : STD_LOGIC_VECTOR (3 DOWNTO 0);
   
--> D�clarez la constante PERIOD qui est utilis�e pour la simulation

   CONSTANT PERIOD    : time := 10 ns;                  --  *** � ajouter avant le premier BEGIN

--> Il faut faire un port map entre vos signaux internes et le component � tester
--> NOTE: Si vous voulez comparer 2 modules VHDL, vous devez g�nr�er 2 port maps 

BEGIN
  -- Par le "port-map" suivant, cela revient � connecter le composant aux signaux internes du tests bench
  -- UUT Unit Under Test: ce nom est habituel mais non impos�.
  -- Si on simule deux composantes, on pourrait avoir UUT1, UUT2 par exemple
  
  UUT: Bin2DualBCD PORT MAP(
    ADCbin => adc_sim,
    Dizaines => sortie_dizaines,
    Unites_ns => sortie_unites_ns,
    Code_signe => sortie_code_signe,
    Unite_s => sortie_unite_s
   );

 
-- *** Test Bench - User Defined Section ***
-- l'int�r�t de cette structure de test bench est que l'on recopie la table de v�rit�.

   tb : PROCESS
   BEGIN
         wait for PERIOD;
         wait for PERIOD; adc_sim <="0000";
         wait for PERIOD; adc_sim <="0001";
         wait for PERIOD; adc_sim <="0010";
         wait for PERIOD; adc_sim <="0011";
         wait for PERIOD; adc_sim <="0100";
         wait for PERIOD; adc_sim <="0101";
         wait for PERIOD; adc_sim <="0110";
         wait for PERIOD; adc_sim <="0111";
         wait for PERIOD; adc_sim <="1000";
         wait for PERIOD; adc_sim <="1001";
         wait for PERIOD; adc_sim <="1010";
         wait for PERIOD; adc_sim <="1011";
         wait for PERIOD; adc_sim <="1100";
         wait for PERIOD; adc_sim <="1101";
         wait for PERIOD; adc_sim <="1110";
         wait for PERIOD; adc_sim <="1111";       
         wait; -- will wait forever
   END PROCESS;
END;


