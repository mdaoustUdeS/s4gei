----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/18/2022 06:08:02 PM
-- Design Name: 
-- Module Name: decodeur3_8_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity decodeur3_8_tb is
--  Port ( );
end decodeur3_8_tb;

architecture Behavioral of decodeur3_8_tb is

component Decodeur3_8 is
    Port ( A2_3 : in STD_LOGIC_VECTOR (2 downto 0);
           LD : out STD_LOGIC_VECTOR (7 downto 0)
         );
end component;

signal A2_3_sim : STD_LOGIC_VECTOR (2 downto 0);
signal LD_sim : STD_LOGIC_VECTOR (7 downto 0);

constant period : time := 50 ns;

begin

    UUT : Decodeur3_8
        port map (
            A2_3 => A2_3_sim,
            LD => LD_sim
        );

   tb : PROCESS
   BEGIN
         wait for PERIOD;
         wait for PERIOD; A2_3_sim <="000";
         wait for PERIOD; A2_3_sim <="001";
         wait for PERIOD; A2_3_sim <="010";
         wait for PERIOD; A2_3_sim <="011";
         wait for PERIOD; A2_3_sim <="100";
         wait for PERIOD; A2_3_sim <="101";
         wait for PERIOD; A2_3_sim <="110";
         wait for PERIOD; A2_3_sim <="111";       
         wait; -- will wait forever
   END PROCESS;

end Behavioral;
