----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/18/2022 03:04:33 AM
-- Design Name: 
-- Module Name: Thermo2Bin_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Thermo2Bin_tb is
--  Port ( );
end Thermo2Bin_tb;

architecture Behavioral of Thermo2Bin_tb is

   COMPONENT Thermo2Bin
    Port ( ADCth : in STD_LOGIC_VECTOR (11 downto 0);
           ADCbin : out STD_LOGIC_VECTOR (3 downto 0);
           erreur : out STD_LOGIC);
   END COMPONENT;

   SIGNAL ADCth_sim : STD_LOGIC_VECTOR (11 downto 0);
   SIGNAL ADCbin_sim : STD_LOGIC_VECTOR (3 downto 0);
   SIGNAL erreur_sim : STD_LOGIC;

   SIGNAL Thermometrique : STD_LOGIC_VECTOR (11 downto 0);

   CONSTANT PERIOD: time := 50 ns;

begin

  UUT: Thermo2Bin PORT MAP(
      ADCth => ADCth_sim,
      ADCbin => ADCbin_sim,
      erreur => erreur_sim
   );

ADCth_sim <= Thermometrique;

   tb : PROCESS
   BEGIN
         wait for PERIOD; Thermometrique <="000000000000"; --> Code normal
         wait for PERIOD; Thermometrique <="000000000001";
         wait for PERIOD; Thermometrique <="000000000011";
         wait for PERIOD; Thermometrique <="000000000111";
         wait for PERIOD; Thermometrique <="000000001111";
         wait for PERIOD; Thermometrique <="000000011111";
         wait for PERIOD; Thermometrique <="000000111111";
         wait for PERIOD; Thermometrique <="000001111111";
         wait for PERIOD; Thermometrique <="000011111111";
         wait for PERIOD; Thermometrique <="000111111111";
         wait for PERIOD; Thermometrique <="001111111111";
         wait for PERIOD; Thermometrique <="011111111111";
         wait for PERIOD; Thermometrique <="111111111111";
         wait for PERIOD;
         wait for PERIOD;
         wait for PERIOD; Thermometrique <="000000000010";  --> Code avec erreur
         wait for PERIOD; Thermometrique <="000000101111";
         wait for PERIOD; Thermometrique <="111100001111";
                  
         WAIT; -- will wait forever
   END PROCESS;

end Behavioral;
