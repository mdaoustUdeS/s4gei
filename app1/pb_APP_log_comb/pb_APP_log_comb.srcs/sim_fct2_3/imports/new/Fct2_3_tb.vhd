----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/17/2022 10:25:34 PM
-- Design Name: 
-- Module Name: Fct2_3_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Fct2_3_tb is
--  Port ( );
end Fct2_3_tb;

architecture Behavioral of Fct2_3_tb is

component Fct2_3 is
    Port ( ADCbin : in STD_LOGIC_VECTOR (3 downto 0);
           A2_3 : out STD_LOGIC_VECTOR (2 downto 0)
         );
end component;


signal ADCbin_sim : STD_LOGIC_VECTOR (3 downto 0);
signal A2_3_sim : STD_LOGIC_VECTOR (2 downto 0);

constant period : time := 50 ns;

begin

    UUT : Fct2_3
        port map (
            ADCbin => ADCbin_sim,
            A2_3 => A2_3_sim
        );

   tb : PROCESS
   BEGIN
         wait for PERIOD;
         wait for PERIOD; ADCbin_sim <="0000";
         wait for PERIOD; ADCbin_sim <="0001";
         wait for PERIOD; ADCbin_sim <="0010";
         wait for PERIOD; ADCbin_sim <="0011";
         wait for PERIOD; ADCbin_sim <="0100";
         wait for PERIOD; ADCbin_sim <="0101";
         wait for PERIOD; ADCbin_sim <="0110";
         wait for PERIOD; ADCbin_sim <="0111";
         wait for PERIOD; ADCbin_sim <="1000";
         wait for PERIOD; ADCbin_sim <="1001";
         wait for PERIOD; ADCbin_sim <="1010";
         wait for PERIOD; ADCbin_sim <="1011";
         wait for PERIOD; ADCbin_sim <="1100";
         wait for PERIOD; ADCbin_sim <="1101";
         wait for PERIOD; ADCbin_sim <="1110";
         wait for PERIOD; ADCbin_sim <="1111";       
         wait; -- will wait forever
   END PROCESS;


end Behavioral;
