----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/17/2022 01:35:18 PM
-- Design Name: 
-- Module Name: Moins_5 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Moins_5 is
    Port ( ADCbin_moins5 : in STD_LOGIC_VECTOR (3 downto 0);
           Moins5_out : out STD_LOGIC_VECTOR (3 downto 0)
           );
end Moins_5;

architecture Behavioral of Moins_5 is

    component Add4Bit is
        Port (
           A_top : in STD_LOGIC_VECTOR (3 downto 0);
           B_top : in STD_LOGIC_VECTOR (3 downto 0);
           C_in_top: in STD_LOGIC;
           Sum_top : out STD_LOGIC_VECTOR (3 downto 0);
           C_out_top : out STD_LOGIC
        );
    end component;

begin
    inst_Add4Bit : Add4Bit
        port map (
            A_top => ADCbin_moins5,
            B_top => "1010",
            C_in_top => '1',
            Sum_top => Moins5_out
        );

end Behavioral;
