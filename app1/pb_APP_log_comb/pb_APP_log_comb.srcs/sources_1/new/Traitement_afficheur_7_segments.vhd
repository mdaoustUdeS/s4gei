----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/17/2022 05:34:53 PM
-- Design Name: 
-- Module Name: Traitement_afficheur_7_segments - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Traitement_afficheur_7_segments is
    Port ( ADCbin_7seg : in STD_LOGIC_VECTOR (3 downto 0);
           Err_7seg : in STD_LOGIC;
           S2_7seg : in STD_LOGIC;
           BTN_7seg : in STD_LOGIC_VECTOR (1 downto 0);
           AFF0_7seg : out STD_LOGIC_VECTOR (3 downto 0);
           AFF1_7seg : out STD_LOGIC_VECTOR (3 downto 0));
end Traitement_afficheur_7_segments;

architecture Behavioral of Traitement_afficheur_7_segments is

    component Bin2DualBCD is
        Port (
            ADCbin : in STD_LOGIC_VECTOR (3 downto 0);
            Dizaines : out STD_LOGIC_VECTOR (3 downto 0);
            Unites_ns : out STD_LOGIC_VECTOR (3 downto 0);
            Code_signe : out STD_LOGIC_VECTOR (3 downto 0);
            Unite_s : out STD_LOGIC_VECTOR (3 downto 0)
        );
    end component;
    
    component Bouton2Bin is
        Port (
            ADCbin : in STD_LOGIC_VECTOR (3 downto 0);
            Dizaines : in STD_LOGIC_VECTOR (3 downto 0);
            Unites_ns : in STD_LOGIC_VECTOR (3 downto 0);
            Code_signe : in STD_LOGIC_VECTOR (3 downto 0);
            Unite_s : in STD_LOGIC_VECTOR (3 downto 0);
            erreur : in STD_LOGIC;
            BTN : in STD_LOGIC_VECTOR (1 downto 0);
            S2 : in STD_LOGIC;
            DAFF0 : out STD_LOGIC_VECTOR (3 downto 0);
            DAFF1 : out STD_LOGIC_VECTOR (3 downto 0)
        );
    end component;
    
    signal Diz, Un, Code_s, Un_s : STD_LOGIC_VECTOR (3 downto 0);

begin
    inst_Bin2DualBCD : Bin2DualBCD
        port map (
            ADCbin => ADCbin_7seg,
            Dizaines => Diz,
            Unites_ns => Un,
            Code_signe => Code_s,
            Unite_s => Un_s
        );
    inst_Bouton2Bin : Bouton2Bin
        port map (
            ADCbin => ADCbin_7seg,
            Dizaines => Diz,
            Unites_ns => Un,
            Code_signe => Code_s,
            Unite_s => Un_s,
            erreur => Err_7seg,
            S2 => S2_7seg,
            BTN => BTN_7seg,
            DAFF0 => AFF0_7seg,
            DAFF1 => AFF1_7seg
        );

end Behavioral;
