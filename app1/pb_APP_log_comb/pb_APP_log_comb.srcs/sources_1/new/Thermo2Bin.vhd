----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/17/2022 10:51:51 PM
-- Design Name: 
-- Module Name: Thermo2Bin - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Thermo2Bin is
    Port ( ADCth : in STD_LOGIC_VECTOR (11 downto 0);
           ADCbin : out STD_LOGIC_VECTOR (3 downto 0);
           erreur : out STD_LOGIC);
end Thermo2Bin;

architecture Behavioral of Thermo2Bin is

    component Add4Bit is
        Port (
           A_top : in STD_LOGIC_VECTOR (3 downto 0);
           B_top : in STD_LOGIC_VECTOR (3 downto 0);
           C_in_top: in STD_LOGIC;
           Sum_top : out STD_LOGIC_VECTOR (3 downto 0);
           C_out_top : out STD_LOGIC
        );
    end component;
    
    function errorCheck(ADCbin_th : std_logic_vector (11 downto 0))
            return std_logic is
        variable temp : std_logic;
        begin
        temp := '0';
        for i in ADCbin_th'length-1 to 0 loop
            if ADCbin_th(i) = '1' then
                temp := '1';
            elsif ADCbin_th(i) = '0' and temp = '1' then
                return '1';
            else
                temp := '0';
            end if;
        end loop;
        return '0';
    end errorCheck;
    
    signal B_top11 : std_logic_vector (3 downto 0) := "0000";
    signal B_top10 : std_logic_vector (3 downto 0) := "0000";
    signal B_top9 : std_logic_vector (3 downto 0) := "0000";
    signal B_top8 : std_logic_vector (3 downto 0) := "0000";
    signal B_top7 : std_logic_vector (3 downto 0) := "0000";
    signal B_top6 : std_logic_vector (3 downto 0) := "0000";
    signal B_top5 : std_logic_vector (3 downto 0) := "0000";
    signal B_top4 : std_logic_vector (3 downto 0) := "0000";
    signal B_top3 : std_logic_vector (3 downto 0) := "0000";
    signal B_top2 : std_logic_vector (3 downto 0) := "0000";
    signal B_top1 : std_logic_vector (3 downto 0) := "0000";
    signal B_top0 : std_logic_vector (3 downto 0) := "0000";
    signal A_top10 : std_logic_vector (3 downto 0);
    signal A_top9 : std_logic_vector (3 downto 0);
    signal A_top8 : std_logic_vector (3 downto 0);
    signal A_top7 : std_logic_vector (3 downto 0);
    signal A_top6 : std_logic_vector (3 downto 0);
    signal A_top5 : std_logic_vector (3 downto 0);
    signal A_top4 : std_logic_vector (3 downto 0);
    signal A_top3 : std_logic_vector (3 downto 0);
    signal A_top2 : std_logic_vector (3 downto 0);
    signal A_top1 : std_logic_vector (3 downto 0);
    signal A_top0 : std_logic_vector (3 downto 0);

begin
    process(ADCth)
    begin
        erreur <=  errorCheck(ADCth);
    end process;
    
    B_top11(0) <= ADCth(11);
    B_top10(0) <= ADCth(10);
    B_top9(0) <= ADCth(9);
    B_top8(0) <= ADCth(8);
    B_top7(0) <= ADCth(7);
    B_top6(0) <= ADCth(6);
    B_top5(0) <= ADCth(5);
    B_top4(0) <= ADCth(4);
    B_top3(0) <= ADCth(3);
    B_top2(0) <= ADCth(2);
    B_top1(0) <= ADCth(1);
    B_top0(0) <= ADCth(0);
    
    inst_Add4Bit11 : Add4Bit
            port map (
            A_top => "0000",
            B_top => B_top11,
            C_in_top => '0',
            Sum_top => A_top10
            );
            
    inst_Add4Bit10 : Add4Bit
            port map (
            A_top => A_top10,
            B_top => B_top10,
            C_in_top => '0',
            Sum_top => A_top9
            );
                
    inst_Add4Bit9 : Add4Bit
            port map (
            A_top => A_top9,
            B_top => B_top9,
            C_in_top => '0',
            Sum_top => A_top8
            );
            
    inst_Add4Bit8 : Add4Bit
            port map (
            A_top => A_top8,
            B_top => B_top8,
            C_in_top => '0',
            Sum_top => A_top7
            );
                
    inst_Add4Bit7 : Add4Bit
            port map (
            A_top => A_top7,
            B_top => B_top7,
            C_in_top => '0',
            Sum_top => A_top6
            );
            
    inst_Add4Bit6 : Add4Bit
            port map (
            A_top => A_top6,
            B_top => B_top6,
            C_in_top => '0',
            Sum_top => A_top5
            );
                
    inst_Add4Bit5 : Add4Bit
            port map (
            A_top => A_top5,
            B_top => B_top5,
            C_in_top => '0',
            Sum_top => A_top4
            );
            
    inst_Add4Bit4 : Add4Bit
            port map (
            A_top => A_top4,
            B_top => B_top4,
            C_in_top => '0',
            Sum_top => A_top3
            );
                
    inst_Add4Bit3 : Add4Bit
            port map (
            A_top => A_top3,
            B_top => B_top3,
            C_in_top => '0',
            Sum_top => A_top2
            );
            
    inst_Add4Bit2 : Add4Bit
            port map (
            A_top => A_top2,
            B_top => B_top2,
            C_in_top => '0',
            Sum_top => A_top1
            );
                
    inst_Add4Bit1 : Add4Bit
            port map (
            A_top => A_top1,
            B_top => B_top1,
            C_in_top => '0',
            Sum_top => A_top0
            );
            
    inst_Add4Bit0 : Add4Bit
            port map (
            A_top => A_top0,
            B_top => B_top0,
            C_in_top => '0',
            Sum_top => ADCbin
            );

end Behavioral;
