----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/16/2022 02:57:03 PM
-- Design Name: 
-- Module Name: Bin2DualBCD_NS - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Bin2DualBCD_NS is
    Port ( 
        ADCbin_ns : in STD_LOGIC_VECTOR (3 downto 0);
        Diz : out STD_LOGIC_VECTOR (3 downto 0);
        Unites : out STD_LOGIC_VECTOR (3 downto 0)
        );
end Bin2DualBCD_NS;

architecture Behavioral of Bin2DualBCD_NS is

function dizaines(ADCbin : std_logic_vector(3 downto 0))
        return STD_LOGIC_VECTOR is
    variable A : STD_LOGIC;
    variable B : STD_LOGIC;
    variable C : STD_LOGIC;
    variable D : STD_LOGIC;
    variable f : STD_LOGIC;
	begin
	A := ADCbin(3);
    B := ADCbin(2);
    C := ADCbin(1);
    D := ADCbin(0);
    f := (A and C) or (A and B);
        if (f = '1') then
            return "0001";
		else
		    return "0000";
        end if;
end dizaines;

function fonctionUnites(ADCbin : std_logic_vector(3 downto 0))
        return STD_LOGIC_VECTOR is
    variable unites : STD_LOGIC_VECTOR (3 downto 0) := "0000";
    variable A : STD_LOGIC;
    variable B : STD_LOGIC;
    variable C : STD_LOGIC;
    variable D : STD_LOGIC;
    variable U1 : STD_LOGIC;
    variable U2 : STD_LOGIC;
    variable U3 : STD_LOGIC;
    variable U4 : STD_LOGIC;
	begin
	A := ADCbin(3);
    B := ADCbin(2);
    C := ADCbin(1);
    D := ADCbin(0);
    U1 := (A and not B and not C);
    U2 := (not A and B) or (B and C);
    U3 := (not A and C) or (A and B and not C);
    U4 := D;
    unites := U1 & U2 & U3 & U4;
    return unites;
end fonctionUnites;

begin
    process(ADCbin_ns)
    begin
        Diz <= dizaines(ADCbin_ns);
        Unites <= fonctionUnites(ADCbin_ns);
    end process;

end Behavioral;
