----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/16/2022 02:38:25 PM
-- Design Name: 
-- Module Name: Bin2DualBCD - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Bin2DualBCD is
    Port ( ADCbin : in STD_LOGIC_VECTOR (3 downto 0);
           Dizaines : out STD_LOGIC_VECTOR (3 downto 0);
           Unites_ns : out STD_LOGIC_VECTOR (3 downto 0);
           Code_signe : out STD_LOGIC_VECTOR (3 downto 0);
           Unite_s : out STD_LOGIC_VECTOR (3 downto 0)
         );
end Bin2DualBCD;

architecture Behavioral of Bin2DualBCD is

    component Bin2DualBCD_NS is
        Port (
            ADCbin_ns : in STD_LOGIC_VECTOR (3 downto 0);
            Diz : out STD_LOGIC_VECTOR (3 downto 0);
            Unites : out STD_LOGIC_VECTOR (3 downto 0)
        );
    end component;
    
    component Bin2BCD_S is
        Port (
            Moins5 : in STD_LOGIC_VECTOR (3 downto 0);
            Moins : out STD_LOGIC_VECTOR (3 downto 0);
            Unit5 : out STD_LOGIC_VECTOR (3 downto 0)
        );
    end component;
    
    component Moins_5 is
        Port (
            ADCbin_moins5 : in STD_LOGIC_VECTOR (3 downto 0);
            Moins5_out : out STD_LOGIC_VECTOR (3 downto 0)
        );
    end component;
    
    signal moins5_signal : STD_LOGIC_VECTOR (3 downto 0);

begin
    inst_bin2dualBCD_NS : Bin2DualBCD_NS
        port map (
            ADCbin_ns => ADCbin,
            Diz => Dizaines,
            Unites => Unites_ns
        );
    inst_moins_5 : Moins_5
        port map (
            ADCbin_moins5 => ADCbin,
            Moins5_out => moins5_signal
        );
    inst_bin2BCD_S : Bin2BCD_S
        port map (
            Moins5 => moins5_signal,
            Moins => Code_signe,
            Unit5 => Unite_s
        );

end Behavioral;
