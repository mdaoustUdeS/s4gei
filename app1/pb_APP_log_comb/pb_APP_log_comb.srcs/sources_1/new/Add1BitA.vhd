----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/16/2022 03:02:38 PM
-- Design Name: 
-- Module Name: Add1BitA - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Add1BitA is
    Port ( A : in STD_LOGIC;
           B : in STD_LOGIC;
           C_in : in STD_LOGIC;
           Sum : out STD_LOGIC;
           C_out : out STD_LOGIC);
end Add1BitA;

architecture Behavioral of Add1BitA is

signal inputs : STD_LOGIC_VECTOR (2 downto 0);

begin
inputs <= A & B & C_in;
    process(inputs)
    begin 
        case inputs is
            when "111" =>
                Sum <= '1';
                C_out <= '1';
            when "001" | "010" | "100" =>
                Sum <= '1';
                C_out <= '0';
            when "011" | "110" | "101" =>
                Sum <= '0';
                C_out <= '1';
            when others =>
                Sum <= '0';
                C_out <= '0';
         end case;
    end process;

end Behavioral;
