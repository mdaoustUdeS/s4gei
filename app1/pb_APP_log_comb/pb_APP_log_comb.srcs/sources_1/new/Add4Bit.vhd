----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/17/2022 01:06:44 PM
-- Design Name: 
-- Module Name: Add4Bit - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Add4Bit is
    Port ( A_top : in STD_LOGIC_VECTOR (3 downto 0);
           B_top : in STD_LOGIC_VECTOR (3 downto 0);
           C_in_top : in STD_LOGIC;
           Sum_top : out STD_LOGIC_VECTOR (3 downto 0);
           C_out_top : out STD_LOGIC
           );
end Add4Bit;

architecture Behavioral of Add4Bit is

signal C1, C2, C3 : STD_LOGIC;

    component Add1BitA is
        Port (
           A : in STD_LOGIC;
           B : in STD_LOGIC;
           C_in : in STD_LOGIC;
           Sum : out STD_LOGIC;
           C_out : out STD_LOGIC
        );
    end component;
    
    component Add1BitB is
        Port (
           A : in STD_LOGIC;
           B : in STD_LOGIC;
           C_in : in STD_LOGIC;
           Sum : out STD_LOGIC;
           C_out : out STD_LOGIC
        );
    end component;

begin
    inst1_Add1BitA : Add1BitA
        port map (
            A => A_top(0),
            B => B_top(0),
            C_in => C_in_top,
            Sum => Sum_top(0),
            C_out => C1
        );
    inst2_Add1BitA : Add1BitA
        port map (
            A => A_top(1),
            B => B_top(1),
            C_in => C1,
            Sum => Sum_top(1),
            C_out => C2
        );
    inst1_Add1BitB : Add1BitB
        port map (
            A => A_top(2),
            B => B_top(2),
            C_in => C2,
            Sum => Sum_top(2),
            C_out => C3
        );
    inst2_Add1BitB : Add1BitB
        port map (
            A => A_top(3),
            B => B_top(3),
            C_in => C3,
            Sum => Sum_top(3),
            C_out => C_out_top
        );

end Behavioral;
