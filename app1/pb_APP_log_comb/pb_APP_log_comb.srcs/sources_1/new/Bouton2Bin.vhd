----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/17/2022 02:17:36 PM
-- Design Name: 
-- Module Name: Bouton2Bin - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Bouton2Bin is
    Port ( ADCbin : in STD_LOGIC_VECTOR (3 downto 0);
           Dizaines : in STD_LOGIC_VECTOR (3 downto 0);
           Unites_ns : in STD_LOGIC_VECTOR (3 downto 0);
           Code_signe : in STD_LOGIC_VECTOR (3 downto 0);
           Unite_s : in STD_LOGIC_VECTOR (3 downto 0);
           erreur : in STD_LOGIC;
           BTN : in STD_LOGIC_VECTOR (1 downto 0);
           S2 : in STD_LOGIC;
           DAFF0 : out STD_LOGIC_VECTOR (3 downto 0);
           DAFF1 : out STD_LOGIC_VECTOR (3 downto 0));
end Bouton2Bin;

architecture Behavioral of Bouton2Bin is

begin
    process(ADCbin, Dizaines, Unites_ns, Code_signe, Unite_s, erreur, BTN, S2)
    begin
        if (erreur = '1' or BTN = "11" or S2 = '1') then
            DAFF0 <= "1111";
            DAFF1 <= "1110";
        elsif (BTN = "00") then
            DAFF1 <= Dizaines;
            DAFF0 <= Unites_ns;
        elsif(BTN = "01") then
            DAFF0 <= ADCbin;
            DAFF1 <= "0000";
        elsif(BTN = "10") then
            DAFF0 <= Unite_s;
            DAFF1 <= Code_signe;
        else
            DAFF0 <= "0000";
            DAFF1 <= "0000";
        end if;
    end process;
end Behavioral;
