----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/17/2022 12:32:09 PM
-- Design Name: 
-- Module Name: Add1BitB - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Add1BitB is
    Port ( A : in STD_LOGIC;
           B : in STD_LOGIC;
           C_in : in STD_LOGIC;
           Sum : out STD_LOGIC;
           C_out : out STD_LOGIC);
end Add1BitB;

architecture Behavioral of Add1BitB is

signal inputs : STD_LOGIC_VECTOR (2 downto 0);

function computeCout(input : STD_LOGIC_VECTOR)
        return STD_LOGIC is
    variable bitA : STD_LOGIC;
    variable bitB : STD_LOGIC;
    variable bitC : STD_LOGIC;
    variable f : STD_LOGIC;
	begin
	bitA := input(2);
	bitB := input(1);
	bitC := input(0);
    f := (bitA and bitB) or (bitC and bitB) or (bitC and bitA);
    return f;
end computeCout;

function computeSum(input : STD_LOGIC_VECTOR)
        return STD_LOGIC is
    variable bitA : STD_LOGIC;
    variable bitB : STD_LOGIC;
    variable bitC : STD_LOGIC;
    variable f : STD_LOGIC;
	begin
	bitA := input(2);
	bitB := input(1);
	bitC := input(0);
    f := (not bitC and not bitA and bitB) or (not bitC and bitA and not bitB) or (bitC and not bitA and not bitB) or (bitC and bitA and bitB);
    return f;
end computeSum;

begin
inputs <= A & B & C_in;
    process(inputs)
    begin
        C_out <= computeCout(inputs);
        Sum <= computeSum(inputs);
    end process;

end Behavioral;
