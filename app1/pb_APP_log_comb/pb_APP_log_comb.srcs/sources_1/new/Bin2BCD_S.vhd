----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/17/2022 02:13:22 PM
-- Design Name: 
-- Module Name: Bin2BCD_S - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Bin2BCD_S is
    Port ( 
           Moins5 : in STD_LOGIC_VECTOR (3 downto 0);
           Moins : out STD_LOGIC_VECTOR (3 downto 0);
           Unit5 : out STD_LOGIC_VECTOR (3 downto 0)
           );
end Bin2BCD_S;

architecture Behavioral of Bin2BCD_S is

begin
    process(Moins5)
    begin
        if (Moins5(3)='1') then
            Moins <= "1101";
            case Moins5 is
                when "1011" =>
                    Unit5 <= "0101";
                when "1100" =>
                    Unit5 <= "0100";
                when "1101" =>
                    Unit5 <= "0011";
                when "1110" =>
                    Unit5 <= "0010";
                when "1111" =>
                    Unit5 <= "0001";
                when others =>
                    Unit5 <= "0000";
            end case;
        else
            Moins <= "0000";
            Unit5 <= Moins5;
        end if;
    end process;

end Behavioral;
