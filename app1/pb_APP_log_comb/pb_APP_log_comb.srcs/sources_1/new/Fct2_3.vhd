----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/16/2022 07:09:18 PM
-- Design Name: 
-- Module Name: Fct2_3 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Fct2_3 is
    Port ( ADCbin : in STD_LOGIC_VECTOR (3 downto 0);
           A2_3 : out STD_LOGIC_VECTOR (2 downto 0)
         );
end Fct2_3;

architecture Behavioral of Fct2_3 is

    component Add4Bit is
        Port (
           A_top : in STD_LOGIC_VECTOR (3 downto 0);
           B_top : in STD_LOGIC_VECTOR (3 downto 0);
           C_in_top: in STD_LOGIC;
           Sum_top : out STD_LOGIC_VECTOR (3 downto 0);
           C_out_top : out STD_LOGIC
        );
    end component;

signal out_carry : STD_LOGIC;
signal Sum_top_trim : STD_LOGIC_VECTOR (3 downto 0);
signal PARTIAL_SUM_1 : STD_LOGIC_VECTOR (3 downto 0);
signal PARTIAL_SUM_3 : STD_LOGIC_VECTOR (3 downto 0);

begin


PARTIAL_SUM_1 <= '0' & ADCbin(3 downto 1);
PARTIAL_SUM_3 <= "000" & ADCbin(3);

    inst_Add4Bit : Add4Bit
        port map (
            A_top => PARTIAL_SUM_1,
            B_top => PARTIAL_SUM_3,
            C_in_top => '0',
            Sum_top => Sum_top_trim,
            C_out_top => out_carry
        );

A2_3 <= Sum_top_trim(2 downto 0);

end Behavioral;
