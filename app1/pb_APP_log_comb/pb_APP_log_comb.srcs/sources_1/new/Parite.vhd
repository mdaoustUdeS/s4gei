----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/19/2022 01:15:52 AM
-- Design Name: 
-- Module Name: Parite - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Parite is
    Port ( ADCbin : in STD_LOGIC_VECTOR (3 downto 0);
           S1 : in STD_LOGIC;
           Parite : out STD_LOGIC
         );
end Parite;

architecture Behavioral of Parite is

begin
    process(ADCbin, S1)
    begin
        case (ADCbin) is
            when "0000" => Parite <= not S1;
            when "0011" => Parite <= not S1;
            when "0101" => Parite <= not S1;
            when "0110" => Parite <= not S1;
            when "1001" => Parite <= not S1;
            when "1010" => Parite <= not S1;
            when "1100" => Parite <= not S1;
            when "1111" => Parite <= not S1;
            when others => Parite <= S1;
        end case;
    end process;
end Behavioral;
