----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/19/2022 01:31:13 AM
-- Design Name: 
-- Module Name: parite_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity parite_tb is
--  Port ( );
end parite_tb;

architecture Behavioral of parite_tb is

component Parite is
    Port ( ADCbin : in STD_LOGIC_VECTOR (3 downto 0);
           S1 : in STD_LOGIC;
           Parite : out STD_LOGIC
         );
end component;

signal ADCbin_sim : STD_LOGIC_VECTOR (3 downto 0);
signal S1_sim : STD_LOGIC;
signal Parite_sim : STD_LOGIC;

constant period : time := 25 ns;

begin

    UUT : Parite
        port map (
            ADCbin => ADCbin_sim,
            S1 => S1_sim,
            Parite => Parite_sim
        );

   tb : PROCESS
   BEGIN
         wait for PERIOD; S1_sim <= '1';
         wait for PERIOD; ADCbin_sim <="0000";
         wait for PERIOD; ADCbin_sim <="0001";
         wait for PERIOD; ADCbin_sim <="0010";
         wait for PERIOD; ADCbin_sim <="0011";
         wait for PERIOD; ADCbin_sim <="0100";
         wait for PERIOD; ADCbin_sim <="0101";
         wait for PERIOD; ADCbin_sim <="0110";
         wait for PERIOD; ADCbin_sim <="0111";
         wait for PERIOD; ADCbin_sim <="1000";
         wait for PERIOD; ADCbin_sim <="1001";
         wait for PERIOD; ADCbin_sim <="1010";
         wait for PERIOD; ADCbin_sim <="1011";
         wait for PERIOD; ADCbin_sim <="1100";
         wait for PERIOD; ADCbin_sim <="1101";
         wait for PERIOD; ADCbin_sim <="1110";
         wait for PERIOD; ADCbin_sim <="1111";
         wait for PERIOD; S1_sim <= '0';
         wait for PERIOD; ADCbin_sim <="0000";
         wait for PERIOD; ADCbin_sim <="0001";
         wait for PERIOD; ADCbin_sim <="0010";
         wait for PERIOD; ADCbin_sim <="0011";
         wait for PERIOD; ADCbin_sim <="0100";
         wait for PERIOD; ADCbin_sim <="0101";
         wait for PERIOD; ADCbin_sim <="0110";
         wait for PERIOD; ADCbin_sim <="0111";
         wait for PERIOD; ADCbin_sim <="1000";
         wait for PERIOD; ADCbin_sim <="1001";
         wait for PERIOD; ADCbin_sim <="1010";
         wait for PERIOD; ADCbin_sim <="1011";
         wait for PERIOD; ADCbin_sim <="1100";
         wait for PERIOD; ADCbin_sim <="1101";
         wait for PERIOD; ADCbin_sim <="1110";
         wait for PERIOD; ADCbin_sim <="1111";
         wait; -- will wait forever
   END PROCESS;

end Behavioral;
