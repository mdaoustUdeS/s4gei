----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/17/2022 10:22:02 PM
-- Design Name: 
-- Module Name: traitement_7_segments - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity traitement_7_segments is
--  Port ( );
end traitement_7_segments;

architecture Behavioral of traitement_7_segments is

component Traitement_afficheur_7_segments
    Port(
           ADCbin_7seg : in STD_LOGIC_VECTOR (3 downto 0);
           Err_7seg : in STD_LOGIC;
           S2_7seg : in STD_LOGIC;
           BTN_7seg : in STD_LOGIC_VECTOR (1 downto 0);
           AFF0_7seg : out STD_LOGIC_VECTOR (3 downto 0);
           AFF1_7seg : out STD_LOGIC_VECTOR (3 downto 0)
    );
end component;

signal ADCbin_sim : std_logic_vector (3 downto 0);
signal err_sim : std_logic := '0';
signal S2_sim : std_logic := '0';
signal BTN_sim : std_logic_vector (1 downto 0) := "00";
signal AFF0_sim : std_logic_vector (3 downto 0);
signal AFF1_sim : std_logic_vector (3 downto 0);

constant period : time := 10ns;

begin

uut: Traitement_afficheur_7_segments port map(
    ADCbin_7seg => ADCbin_sim,
    Err_7seg => err_sim,
    S2_7seg => S2_sim,
    BTN_7seg => BTN_sim,
    AFF0_7seg => AFF0_sim,
    AFF1_7seg => AFF1_sim
);

tb : process
begin
    wait for period;
         wait for PERIOD; ADCbin_sim <="0000";
         wait for PERIOD; ADCbin_sim <="0001";
         wait for PERIOD; ADCbin_sim <="0010";
         wait for PERIOD; ADCbin_sim <="0011";
         wait for PERIOD; ADCbin_sim <="0100";
         wait for PERIOD; ADCbin_sim <="0101";
         wait for PERIOD; ADCbin_sim <="0110";
         wait for PERIOD; ADCbin_sim <="0111";
         wait for PERIOD; ADCbin_sim <="1000";
         wait for PERIOD; ADCbin_sim <="1001";
         wait for PERIOD; ADCbin_sim <="1010";
         wait for PERIOD; ADCbin_sim <="1011";
         wait for PERIOD; ADCbin_sim <="1100";
         wait for PERIOD; ADCbin_sim <="1101";
         wait for PERIOD; ADCbin_sim <="1110";
         wait for PERIOD; ADCbin_sim <="1111";
         wait for PERIOD; err_sim <='1';
         wait for PERIOD; S2_sim <='1';
         wait for PERIOD; BTN_sim <="01";
         wait for PERIOD; BTN_sim <="10";
         wait for PERIOD; BTN_sim <="11";
    wait;
end process;
end Behavioral;
