---
type: slide
slideOptions:
  transition: slide
---

# Système sensoriel chez l'humain

Par Matthieu Daoust [8h] et Antoine Labrie [8h]

---

## 1. Alerte (5 diapo)

### Alerte: Contexte d’utilisation

Le bruit de la voiture a une fréquence grave (moins de 350Hz).

---

### Alerte: Spécification de l'utilisateur

- L'utilisateur est un propriétaire de voiture.
- Dans notre scénario, l'utilisateur se fait avertir d'une défaillance par une alerte sonore pendant qu'il est au volant de son véhicule. Il ne doit pas sursauter à l'écoute de l'alerte. Aussi, il doit bien entendre l'alerte malgré les bruits ambiants.
- Nous avons utilisé la méthode des limites de Fechner pour évaluer la courbe de seuil d'audition.

---

### Alerte: Courbes d’audition de l'utilisateur

![](./img/courbe-seuil.png)

> Figure, interprétation et analyse
>
---

### Alerte: Prototype

- Choix de conception pour la bonne l’alerte sonore incluant le calcul de la modulation par amplitude causant le battement (Voir fig.11, Sensation et perception)
- Choix de conception pour l’alerte sonore désagréable

---

### Alerte: Evaluation du design selon les requis

![](./img/figure-1.png)

---

## 2. Implant cochléaire (3 diapo)

### Solutions pour 4 et 16 canaux

---

### Protocole d'évaluation de l’implant

- Description des métriques

---

### Protocole d'évaluation de l’implant (suite)

- Solutions pour 4 et 16 canaux (Incluant les résultats de l’évaluation des solution et justifications du choix de la solution)

---

## 3. Persistance visuelle (2 diapo)

### Décrire votre protocole pour déterminer le seuil d’interférences visuelles

---

### Quelle est la fréquence la plus élevée à laquelle vous pouvez observer le stimulus sans interférences?

### Interprétez vos observations en lien avec la structure du système visuel

> Indice: Voir Sensation et perception

---

## 4. Mouvement (1 diapo)

### Pourquoi le systèmme visuel en arrive-t-il à percevoir plusieurs objets visuels à partir du mouvement?

> Indice: Prédominance des certaines orientation dans V1

---

## 5. Cohérence audiovisuelle (2 diapo)

### Protocole de mesure de la perception de la synchronie

> Inclure une figure présentant vos résultats (Moyenne et écart-type)

---

### Mécanismes de sensation et perception qui peuvent expliquer l’existence du seuil

> Indice: comparer le nombre de synapse que le signal parcours jusqu'à leur cortex respectif. (Chemin de données)

---

## 6. Modulation (2 diapo)

| Fréquence | Moyenne |
| --------- | ------- |
| 500Hz     | 6 Hz    |
| 200Hz     | 30 Hz   |

Tableau résumant les mesures de la limite de la perception de l’effet de battement

---

### Explication du phénomène de perception acoustique de battement/rotation

---

## 6. Sonification de trajectoire (3 diapo)

### Résumé du protocole (choix, modifications, exemples de trajectoires)

---

### Impact des paramètres de sonification

---

### Présenter et justifier vos recommandations de paramètres de sonification

---

# Références

- Notes de cours
- Procéduraux
- Laboratoire
