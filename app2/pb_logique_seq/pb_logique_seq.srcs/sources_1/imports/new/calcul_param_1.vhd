
---------------------------------------------------------------------------------------------
--    calcul_param_1.vhd
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
--    Universit� de Sherbrooke - D�partement de GEGI
--
--    Version         : 5.0
--    Nomenclature    : inspiree de la nomenclature 0.2 GRAMS
--    Date            : 16 janvier 2020, 4 mai 2020
--    Auteur(s)       : 
--    Technologie     : ZYNQ 7000 Zybo Z7-10 (xc7z010clg400-1) 
--    Outils          : vivado 2019.1 64 bits
--
---------------------------------------------------------------------------------------------
--    Description (sur une carte Zybo)
---------------------------------------------------------------------------------------------
--
---------------------------------------------------------------------------------------------
-- � FAIRE: 
-- Voir le guide de la probl�matique
---------------------------------------------------------------------------------------------
--
---------------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;  -- pour les additions dans les compteurs
USE ieee.numeric_std.ALL;
Library UNISIM;
use UNISIM.vcomponents.all;

----------------------------------------------------------------------------------
-- 
----------------------------------------------------------------------------------
entity calcul_param_1 is
    Port (
    i_bclk    : in   std_logic; -- bit clock (I2S)
    i_reset   : in   std_logic;
    i_en      : in   std_logic; -- un echantillon present a l'entr�e
    i_ech     : in   std_logic_vector (23 downto 0); -- echantillon en entr�e
    o_param   : out  std_logic_vector (7 downto 0)   -- param�tre calcul�
    );
end calcul_param_1;

----------------------------------------------------------------------------------

architecture Behavioral of calcul_param_1 is

component compteur_nbits is
generic (nbits : integer := 8);
    port (
        clk             : in    std_logic; 
        i_en            : in    std_logic; 
        reset           : in    std_logic; 
        o_val_cpt       : out   std_logic_vector (nbits-1 downto 0)
    );
end component;

---------------------------------------------------------------------------------
-- Signaux
----------------------------------------------------------------------------------

signal output : std_logic_vector (7 downto 0) := "00000000";
signal en_compteur : std_logic := '0';
signal reset_compteur : std_logic := '1';
signal compteur : std_logic_vector (7 downto 0);

type state_type is (A, B, C, D);
signal current_state, next_state : state_type;

---------------------------------------------------------------------------------------------
--    Description comportementale
---------------------------------------------------------------------------------------------
begin

inst_compteur_nbits : compteur_nbits
    generic map (nbits => 8)
    port map (
        clk => i_bclk,
        i_en => en_compteur,
        reset => reset_compteur,
        o_val_cpt => compteur
    );

    process (i_bclk) -- state memory with synchronous reset
        begin
        if i_bclk'event and i_bclk= '1' then
        if i_reset= '1' then current_state <= A; reset_compteur <= '1';
        else
        current_state <= next_state; 
        end if;
        end if ;
    end process ;
    
    process (i_en) --next-state logic
    begin
        case current_state is
            when A =>   if i_ech(23) = '0' and i_en = '1'     then    next_state <= B; en_compteur <= '1'; reset_compteur <= '0';
                        elsif i_ech(23) = '1' and i_en = '1'  then    next_state <= A; en_compteur <= '1'; reset_compteur <= '0';
                        else                                          next_state <= A; en_compteur <= '0'; reset_compteur <= '0';
                        end if;
            when B =>   if i_ech(23) = '0' and i_en = '1'     then    next_state <= C; en_compteur <= '1'; reset_compteur <= '0';
                        elsif i_ech(23) = '1' and i_en = '1'  then    next_state <= A; en_compteur <= '1'; reset_compteur <= '0';
                        else                                          next_state <= B; en_compteur <= '0'; reset_compteur <= '0';
                        end if;
            when C =>   if i_ech(23) = '0' and i_en = '1'     then    next_state <= D; en_compteur <= '1'; reset_compteur <= '1'; output <= "00000001";
                        elsif i_ech(23) = '1' and i_en = '1'  then    next_state <= A; en_compteur <= '1'; reset_compteur <= '0';
                        else                                          next_state <= C; en_compteur <= '0'; reset_compteur <= '0';
                        end if;
            when D =>   if i_ech(23) = '0' and i_en = '1'     then    next_state <= D; en_compteur <= '1'; reset_compteur <= '0';
                        elsif i_ech(23) = '1' and i_en = '1'  then    next_state <= A; en_compteur <= '1'; reset_compteur <= '0';
                        else                                          next_state <= D; en_compteur <= '0'; reset_compteur <= '0';
                        end if;
            when others => next_state <= A;
        end case;
    end process;
    
    o_param <= output;
 
end Behavioral;
