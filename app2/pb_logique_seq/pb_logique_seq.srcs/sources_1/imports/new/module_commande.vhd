--  module_commande.vhd
--  D. Dalle  30 avril 2019, 16 janv 2020, 23 avril 2020
--  module qui permet de r�unir toutes les commandes (problematique circuit sequentiels)
--  recues des boutons, avec conditionnement, et des interrupteurs

-- 23 avril 2020 elimination constante mode_seq_bouton: std_logic := '0'

LIBRARY ieee;
USE ieee.std_logic_1164.all;

entity module_commande IS
generic (nbtn : integer := 4;  mode_simulation: std_logic := '0');
    PORT (
          clk              : in  std_logic;
          o_reset          : out  std_logic; 
          i_btn            : in  std_logic_vector (nbtn-1 downto 0); -- signaux directs des boutons
          i_sw             : in  std_logic_vector (3 downto 0);      -- signaux directs des interrupteurs
          o_btn_cd         : out std_logic_vector (nbtn-1 downto 0); -- signaux conditionn�s 
          o_selection_fct  :  out std_logic_vector(1 downto 0);
          o_selection_par  :  out std_logic_vector(1 downto 0)
          );
end module_commande;

ARCHITECTURE BEHAVIOR OF module_commande IS


component conditionne_btn_v7 is
generic (nbtn : integer := nbtn;  mode_simul: std_logic := '0');
    port (
         CLK          : in std_logic;         -- devrait etre de l ordre de 50 Mhz
         i_btn        : in    std_logic_vector (nbtn-1 downto 0);
         --
         o_btn_db     : out    std_logic_vector (nbtn-1 downto 0);
         o_strobe_btn : out    std_logic_vector (nbtn-1 downto 0)
         );
end component;


    signal d_strobe_btn :    std_logic_vector (nbtn-1 downto 0);
    signal d_btn_cd     :    std_logic_vector (nbtn-1 downto 0); 
    signal d_reset      :    std_logic;
    
    type state_type is (A, B, C, D);
    signal current_state, next_state : state_type;
    
    signal button_state : std_logic_vector (1 downto 0) := "00";
   
   
BEGIN
                  
 inst_cond_btn:  conditionne_btn_v7
    generic map (nbtn => nbtn, mode_simul => mode_simulation)
    port map(
        clk           => clk,
        i_btn         => i_btn,
        o_btn_db      => d_btn_cd,
        o_strobe_btn  => d_strobe_btn  
         );
         
    process (clk) -- state memory with synchronous reset
        begin
        if clk'event and clk= '1' then
        if d_reset= '1' then current_state <= A;
        else
        current_state <= next_state; end if;
        end if ;
    end process ;
    
    process (d_btn_cd, current_state) --next-state logic
    begin
        if (button_state = d_btn_cd(1 downto 0)) then
            next_state <= current_state;
        else
            button_state <= d_btn_cd(1 downto 0);
            case current_state is
                when A =>   if      d_btn_cd(1) = '0' and d_btn_cd(0) = '1'   then    next_state <= B;
                            elsif   d_btn_cd(1) = '1' and d_btn_cd(0) = '0'   then    next_state <= D;
                            else                                                next_state <= current_state;
                            end if;
                when B =>   if      d_btn_cd(1) = '0' and d_btn_cd(0) = '1'   then    next_state <= C;
                            elsif   d_btn_cd(1) = '1' and d_btn_cd(0) = '0'   then    next_state <= A;
                            else                                                next_state <= current_state;
                            end if;
                when C =>   if      d_btn_cd(1) = '0' and d_btn_cd(0) = '1'   then    next_state <= D;
                            elsif   d_btn_cd(1) = '1' and d_btn_cd(0) = '0'   then    next_state <= B;
                            else                                                next_state <= current_state;
                            end if;
                when D =>   if      d_btn_cd(1) = '0' and d_btn_cd(0) = '1'   then    next_state <= A;
                            elsif   d_btn_cd(1) = '1' and d_btn_cd(0) = '0'   then    next_state <= C;
                            else                                                next_state <= current_state;
                            end if;
                when others => next_state <= A;
            end case;
        end if;
    end process;
    
    with current_state select --output logic
        o_selection_fct <=  "00" when A,
                            "01" when B,
                            "10" when C,
                            "11" when D,
                            "00" when others;
 
   o_btn_cd        <= d_btn_cd;
   o_selection_par <= i_sw(1 downto 0); -- mode de selection du parametre par sw
   --o_selection_fct <= i_sw(3 downto 2); -- mode de selection de la fonction par sw
   d_reset         <= i_btn(3);         -- pas de contionnement particulier sur reset
   o_reset         <= d_reset;          -- pas de contionnement particulier sur reset

END BEHAVIOR;
