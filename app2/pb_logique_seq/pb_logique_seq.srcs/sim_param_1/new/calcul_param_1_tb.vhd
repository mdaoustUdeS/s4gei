----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/01/2022 07:59:32 AM
-- Design Name: 
-- Module Name: calcul_param_1_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity calcul_param_1_tb is
--  Port ( );
end calcul_param_1_tb;

architecture Behavioral of calcul_param_1_tb is

component calcul_param_1
Port (
    i_bclk    : in   std_logic; -- bit clock (I2S)
    i_reset   : in   std_logic;
    i_en      : in   std_logic; -- un echantillon present a l'entr�e
    i_ech     : in   std_logic_vector (23 downto 0); -- echantillon en entr�e
    o_param   : out  std_logic_vector (7 downto 0)   -- param�tre calcul�
    );
end component;

    signal   d_clk_p       :  std_logic := '0';
    signal   d_reset       :  std_logic := '0';
    signal   enable        :  std_logic := '0';
    signal   sim_ech       :  std_logic_vector (23 downto 0);
    signal   sim_param     :  std_logic_vector (7 downto 0);
    
    constant c_clk_p_Period      : time :=  20 ns;
    constant period              : time :=  30 us;

begin

uut : calcul_param_1
port map (
    i_bclk  => d_clk_p,
    i_reset => d_reset,
    i_en    => enable,
    i_ech   => sim_ech,
    o_param => sim_param
);

sim_clk_p :  process
   begin
      d_clk_p <= '1';  -- init
      loop
         wait for c_clk_p_Period / 2;
         d_clk_p <= not d_clk_p; -- invert clock value
      end loop;
   end process;  

tb : process
    begin
        wait for period;
        sim_ech <= "111111111111111111111111"; enable <= '1'; wait for period; enable <= '0'; wait for period;
        sim_ech <= "111111111111111111111111"; enable <= '1'; wait for period; enable <= '0'; wait for period;
        sim_ech <= "111111111111111111111111"; enable <= '1'; wait for period; enable <= '0'; wait for period;
        sim_ech <= "111111111111111111111111"; enable <= '1'; wait for period; enable <= '0'; wait for period;
        sim_ech <= "111111111111111111111111"; enable <= '1'; wait for period; enable <= '0'; wait for period;
        sim_ech <= "011111111111111111111111"; enable <= '1'; wait for period; enable <= '0'; wait for period;
        sim_ech <= "011111111111111111111111"; enable <= '1'; wait for period; enable <= '0'; wait for period;
        sim_ech <= "011111111111111111111111"; enable <= '1'; wait for period; enable <= '0'; wait for period;
        sim_ech <= "011111111111111111111111"; enable <= '1'; wait for period; enable <= '0'; wait for period;
        sim_ech <= "011111111111111111111111"; enable <= '1'; wait for period; enable <= '0'; wait for period;
        sim_ech <= "111111111111111111111111"; enable <= '1'; wait for period; enable <= '0'; wait for period;
        sim_ech <= "111111111111111111111111"; enable <= '1'; wait for period; enable <= '0'; wait for period;
        sim_ech <= "111111111111111111111111"; enable <= '1'; wait for period; enable <= '0'; wait for period;
        sim_ech <= "111111111111111111111111"; enable <= '1'; wait for period; enable <= '0'; wait for period;
        sim_ech <= "111111111111111111111111"; enable <= '1'; wait for period; enable <= '0'; wait for period;
        sim_ech <= "011111111111111111111111"; enable <= '1'; wait for period; enable <= '0'; wait for period;
        sim_ech <= "011111111111111111111111"; enable <= '1'; wait for period; enable <= '0'; wait for period;
        sim_ech <= "011111111111111111111111"; enable <= '1'; wait for period; enable <= '0'; wait for period;
        sim_ech <= "011111111111111111111111"; enable <= '1'; wait for period; enable <= '0'; wait for period;
        sim_ech <= "011111111111111111111111"; enable <= '1'; wait for period; enable <= '0'; wait for period;
    wait;
    end process;

end Behavioral;
