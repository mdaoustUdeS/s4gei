package code;

/** @author Ahmed Khoumsi */

import java.util.ArrayList;

/** Cette classe identifie les terminaux reconnus et retournes par
 *  l'analyseur lexical
 */
public class Terminal {

  public String type;
  public String ul;

  public Terminal(String type, String ul) {   // arguments possibles
    this.type = type;
    this.ul = ul;
  }

}
