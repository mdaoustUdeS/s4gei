package code;

/** @author Ahmed Khoumsi */

/** Classe representant une feuille d'AST
 */
public class FeuilleAST extends ElemAST {

    Terminal terminal;

/**Constructeur pour l'initialisation d'attribut(s)
 */
  public FeuilleAST(Terminal t) {
    terminal = t;
  }

  /** Evaluation de feuille d'AST
   */
  public int EvalAST() {
      return Integer.parseInt(terminal.ul);
  }

  public boolean canEvaluate() {
      if (terminal.type == "nb") {
          return true;
      } else {
          return false;
      }
  }

 /** Lecture de chaine de caracteres correspondant a la feuille d'AST
  */
  public String LectAST( ) {
    return terminal.ul;
  }
}
