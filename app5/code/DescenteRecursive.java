package code;

/** @author Ahmed Khoumsi */

/** Cette classe effectue l'analyse syntaxique
 */
public class DescenteRecursive {

  private String expression;
  AnalLex lexical;

/** Constructeur de DescenteRecursive :
      - recoit en argument le nom du fichier contenant l'expression a analyser
      - pour l'initalisation d'attribut(s)
 */
public DescenteRecursive(String in) {
  expression = in;
  lexical = new AnalLex(expression);
}


/** AnalSynt() effectue l'analyse syntaxique et construit l'AST.
 *    Elle retourne une reference sur la racine de l'AST construit
 */
public ElemAST AnalSynt( ) {
  ElemAST lastNode = null;
  if (lexical.resteTerminal()) {
    lastNode = E(lexical.prochainTerminal());
  }

  return lastNode;
}

private ElemAST E(Terminal terminal) {
  ElemAST leftNode = T(terminal);

  if (lexical.resteTerminal()) {
    Terminal tOps = lexical.prochainTerminal();
    if (tOps.type == "opP2") {
      if (lexical.resteTerminal()) {
        Terminal t2 = lexical.prochainTerminal();
        if (t2.type != "nb" && t2.type != "id") {
          ErreurSynt("Un opérateur doit être suivi d'un nombre ou d'un identificateur");
          System.exit(1);
        }
        ElemAST rightNode = E(t2);
        leftNode = new NoeudAST(tOps, leftNode, rightNode);
      } else {
        ErreurSynt("Erreur de syntaxe, une expression ne doit pas terminer par une opération");
        System.exit(1);
      }
    } else {
      lexical.precedentTerminal(tOps);
    }
  }
  return leftNode;
}

private ElemAST T(Terminal terminal) {
  ElemAST leftNode = F(terminal);

  if (lexical.resteTerminal()) {
    Terminal tOps = lexical.prochainTerminal();
    if (tOps.type == "opP1") {
      if (lexical.resteTerminal()) {
        Terminal t2 = lexical.prochainTerminal();
        if (t2.type != "nb" && t2.type != "id") {
          ErreurSynt("Un opérateur doit être suivi d'un nombre ou d'un identificateur");
          System.exit(1);
        }
        ElemAST rightNode = T(t2);
        leftNode = new NoeudAST(tOps, leftNode, rightNode);
      } else {
        ErreurSynt("Erreur de syntaxe, une expression ne doit pas terminer par une opération");
        System.exit(1);
      }
    } else {
      lexical.precedentTerminal(tOps);
    }
  }
  return leftNode;
}

private ElemAST F(Terminal terminal) {
  if (terminal.type == "nb") {
    return new FeuilleAST(terminal);
  } else if (terminal.type == "id") {
    return new FeuilleAST(terminal);
  } else if (terminal.type == "op(") {
    if(lexical.resteTerminal()) {
      ElemAST lastnode = E(lexical.prochainTerminal());
      if(lexical.resteTerminal()) {
        Terminal t = lexical.prochainTerminal();
        if (t.type == "op)") {
          return lastnode;
        }
      } else {
        ErreurSynt("Manque paranthèse fermante");
        System.exit(1);
      }
    } else {
      ErreurSynt("Expression terminant par une paranthèse ouvrante");
      System.exit(1);
    }
  } else if (terminal.type == "op)") {
    ErreurSynt("Paranthèse fermante sans paranthèse ouvrante");
    System.exit(1);
  }
  return new FeuilleAST(new Terminal("empty","0"));
}

/** ErreurSynt() envoie un message d'erreur syntaxique
 */
public void ErreurSynt(String s)
{
  System.out.println(s);
}



  //Methode principale a lancer pour tester l'analyseur syntaxique
  public static void main(String[] args) {
    String toWriteLect = "";
    String toWriteEval = "";

    System.out.println("Debut d'analyse syntaxique");
    if (args.length == 0){
      args = new String [2];
      args[0] = "ExpArith.txt";
      args[1] = "ResultatSyntaxique.txt";
    }
    Reader r = new Reader(args[0]);
    DescenteRecursive dr = new DescenteRecursive(r.toString());
    try {
      ElemAST RacineAST = dr.AnalSynt();
      toWriteLect += "Lecture de l'AST trouve : " + RacineAST.LectAST() + "\n";
      System.out.println(toWriteLect);
      toWriteEval += "Evaluation de l'AST trouve : " + RacineAST.EvalAST() + "\n";
      System.out.println(toWriteEval);
      Writer w = new Writer(args[1],toWriteLect+toWriteEval); // Ecriture de toWrite
                                                              // dans fichier args[1]
    } catch (Exception e) {
      System.out.println(e);
      e.printStackTrace();
      System.exit(51);
    }
    System.out.println("Analyse syntaxique terminee");
  }

}
