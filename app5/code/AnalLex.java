package code;

/** @author Ahmed Khoumsi */

/** Cette classe effectue l'analyse lexicale
 */
public class AnalLex {

// Attributs
  String string;
  int firstChar;

/** Constructeur pour l'initialisation d'attribut(s)
 */
  public AnalLex(String s) {  // arguments possibles
    string = s;
    firstChar = 0;
  }

/** resteTerminal() retourne :
      false  si tous les terminaux de l'expression arithmetique ont ete retournes
      true s'il reste encore au moins un terminal qui n'a pas ete retourne
 */
  public boolean resteTerminal() {
    if(firstChar >= string.length())
      return false;
    else
      return true;
  }

/** prochainTerminal() retourne le prochain terminal
      Cette methode est une implementation d'un AEF
 */
  public Terminal prochainTerminal() {
    String state = "INIT";
    for(int i = firstChar; i < string.length(); i++) {
      char currentChar = string.charAt(i);
      switch (state) {

        case "INIT":
          if (isOperationP1(currentChar)) {
            Terminal terminal = new Terminal("opP1", string.substring(firstChar, i+1));
            firstChar++;
            return terminal;
          } else if (isOperationP2(currentChar)) {
            Terminal terminal = new Terminal("opP2", string.substring(firstChar, i+1));
            firstChar++;
            return terminal;
          } else if (currentChar == '(') {
            firstChar++;
            return new Terminal("op(", "(");
          } else if (currentChar == ')') {
            firstChar++;
            return new Terminal("op)", ")");
          } else if (isNumber(currentChar)) {
            state = "NUMBER";
          } else if (isUppercase(currentChar)) {
            state = "VARIABLE";
          } else {
            ErreurLex("Erreur lexicale au caractère " + i + " (" + currentChar + ")");
          }
          break;

        case "NUMBER":
          if(isNumber(currentChar)) {
            state = "NUMBER";
          } else {
            Terminal terminal = new Terminal("nb", string.substring(firstChar, i));
            firstChar = i;
            return terminal;
          }
          break;

        case "VARIABLE":
          if (isUppercase(currentChar) | isLowercase(currentChar)) {
            state = "VARIABLE";
          } else if (currentChar == '_') {
            state = "UNDERSCORE";
          } else {
            Terminal terminal = new Terminal("id", string.substring(firstChar, i));
            firstChar = i;
            return terminal;
          }
          break;

        case "UNDERSCORE":
          if (isUppercase(currentChar) | isLowercase(currentChar)) {
            state = "VARIABLE";
          } else {
            ErreurLex("Erreur lexicale au caractère " + i + ": Un '_' ne peut ni être suivi par un autre '_', ni être le dernier élément de la séquence.");
          }
          break;

        default:
          System.out.println("Impossible state");
      }
    }
    int i = string.length();
    Terminal terminal = new Terminal("nb", string.substring(firstChar, i));
    firstChar = i;
    return terminal;
  }

  private boolean isOperationP1(char c) {
    if(c == '*' | c == '/')
      return true;
    else
      return false;
  }

  private boolean isOperationP2(char c) {
    if(c == '+' | c == '-')
      return true;
    else
      return false;
  }

  private boolean isNumber(char c) {
    if(c >= '0' & c <= '9')
      return true;
    else
      return false;
  }

  private boolean isUppercase(char c) {
    if(c >= 'A' & c <= 'Z')
      return true;
    else
      return false;
  }

  private boolean isLowercase(char c) {
    if(c >= 'a' & c <= 'z')
      return true;
    else
      return false;
  }

  public void precedentTerminal(Terminal terminal) {
    firstChar -= terminal.ul.length();
  }

/** ErreurLex() envoie un message d'erreur lexicale
 */
  public void ErreurLex(String s) {
     System.out.println(s);
  }


  //Methode principale a lancer pour tester l'analyseur lexical
  public static void main(String[] args) {
    String toWrite = "";
    System.out.println("Debut d'analyse lexicale");
    if (args.length == 0){
    args = new String [2];
            args[0] = "ExpArith.txt";
            args[1] = "ResultatLexical.txt";
    }
    Reader r = new Reader(args[0]);

    AnalLex lexical = new AnalLex(r.toString()); // Creation de l'analyseur lexical

    // Execution de l'analyseur lexical
    Terminal t = null;
    while(lexical.resteTerminal()){
      t = lexical.prochainTerminal();
      toWrite += t.ul + "\n" ;  // toWrite contient le resultat
    }				   //    d'analyse lexicale
    System.out.println(toWrite); 	// Ecriture de toWrite sur la console
    Writer w = new Writer(args[1],toWrite); // Ecriture de toWrite dans fichier args[1]
    System.out.println("Fin d'analyse lexicale");
  }
}
