package code;

/** @author Ahmed Khoumsi */

/** Classe representant une feuille d'AST
 */
public class NoeudAST extends ElemAST {

  Terminal operation;
  ElemAST leftChild;
  ElemAST rightChild;

  /** Constructeur pour l'initialisation d'attributs
   */
  public NoeudAST(Terminal operation, ElemAST leftChild, ElemAST rightChild) {
    this.operation = operation;
    this.leftChild = leftChild;
    this.rightChild = rightChild;
  }


  /** Evaluation de noeud d'AST
   */
  public int EvalAST( ) {
    if(leftChild.canEvaluate() && rightChild.canEvaluate()) {
      switch(operation.ul) {
        case "+":
          return leftChild.EvalAST() + rightChild.EvalAST();
        case "-":
          return leftChild.EvalAST() - rightChild.EvalAST();
        case "*":
          return leftChild.EvalAST() * rightChild.EvalAST();
        case "/":
          return leftChild.EvalAST() / rightChild.EvalAST();
        default:
          return 0;
      }
    } else {
      super.ErreurEvalAST("Impossible d'évaluer le résultat ne sera pas bon");
      return 0;
    }
  }

  public boolean canEvaluate() {
    return true;
  }

  /** Lecture de noeud d'AST
   */
  public String LectAST( ) {
    return leftChild.LectAST() + " " + rightChild.LectAST() + " " + operation.ul;
  }

}
